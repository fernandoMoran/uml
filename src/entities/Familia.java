/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fernando
 */
public class Familia {
    //Atributos
    private int codigo;
    private String apellido;
    private String direccion;
    private String telefono;
    private List<Mascota> mascotas;
    
    //constructor    
    public Familia() {
    }

    public Familia(int codigo, String apellido, String direccion, String telefono) {
        this.codigo = codigo;
        this.apellido = apellido;
        this.direccion = direccion;
        this.telefono = telefono;
        mascotas = new ArrayList<>();
    }
    
    //Métodos
    public void agregarMiembro(Persona miembro){}
    public void borrarMiembro(String DNI){};
    public void agregarMascota(Mascota mascota){}
    public void borrarMascota(int codigo){}
    
    
    //Getters y settters
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public List<Mascota> getMascotas() {
        return mascotas;
    }

    public void setMascotas(List<Mascota> mascotas) {
        this.mascotas = mascotas;
    }
    
    
}
