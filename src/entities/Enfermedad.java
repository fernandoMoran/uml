/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Fernando
 */
public class Enfermedad {
    //Atributos
    private String nombre;
    
    //Constructores
    public Enfermedad() {
    }

    public Enfermedad(String nombre) {
        this.nombre = nombre;
    }
    
    //Getters y setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
