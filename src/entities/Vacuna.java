/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author Fernando
 */
public class Vacuna {
    //Atributos
    private Enfermedad enfermedad;
    private Date fecha;
    
    //Constructores
    public Vacuna() {
    }

    public Vacuna(Enfermedad enfermedad, Date fecha) {
        this.enfermedad = enfermedad;
        this.fecha = fecha;
    }
    
    //Getters y setters

    public Enfermedad getEnfermedad() {
        return enfermedad;
    }

    public void setEnfermedad(Enfermedad enfermedad) {
        this.enfermedad = enfermedad;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
}
