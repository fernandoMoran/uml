/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author Fernando
 */
public class Mascota {
    //Atributos
    private int codigo;
    private int codigoDuenyo;
    private String alias;
    private String  especie;
    private String raza;
    private Date fechaNacimiento;
    private float pesoMedio;
    private float pesoActual;
    private float[] pesoRegistro;

    public Historial historial;
    public Calendario vacunas;
    
    //Constructores
    public Mascota() {
    }

    public Mascota(int codigo, int codigoDuenyo, String alias, String especie, String raza, Date fechaNacimiento, float pesoMedio, float pesoActual, float[] pesoRegistro, Historial historial, Calendario vacunas) {
        this.codigo = codigo;
        this.codigoDuenyo = codigoDuenyo;
        this.alias = alias;
        this.especie = especie;
        this.raza = raza;
        this.fechaNacimiento = fechaNacimiento;
        this.pesoMedio = pesoMedio;
        this.pesoActual = pesoActual;
        this.pesoRegistro = pesoRegistro;
        this.historial = historial;
        this.vacunas = vacunas;
    }
    
    
    //Métodos
    private float calcularPesoMedio() { return 0; }
    private void guardarPesoRegistro() {}
    public void agregarPeso(){}
    
    //Getters y setters
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigoDuenyo() {
        return codigoDuenyo;
    }

    public void setCodigoDuenyo(int codigoDuenyo) {
        this.codigoDuenyo = codigoDuenyo;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public float getPesoMedio() {
        return pesoMedio;
    }

    public void setPesoMedio(float pesoMedio) {
        this.pesoMedio = pesoMedio;
    }

    public float getPesoActual() {
        return pesoActual;
    }

    public void setPesoActual(float pesoActual) {
        this.pesoActual = pesoActual;
    }

    public float[] getPesoRegistro() {
        return pesoRegistro;
    }

    public void setPesoRegistro(float[] pesoRegistro) {
        this.pesoRegistro = pesoRegistro;
    }
    
    
}
