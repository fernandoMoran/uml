/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fernando
 */
public class Veterinario {
    //Atributos
    private List<Mascota> pacientes;
    private List<Familia> clientes;
    
    //Constructor
    public Veterinario() {
        pacientes = new ArrayList<>();
        clientes = new ArrayList<>();
    }
    
    
    //Métodos
    public void agregarCliente(Familia cliente) {}
    public void borrarCliente(int codigo){}
    public void agregarPaciente(Mascota paciente) {}
    public void borrarPaciente(int codigo) {}
    
    //Getters y setters
    public List<Mascota> getPacientes() {
        return pacientes;
    }

    public void setPacientes(List<Mascota> pacientes) {
        this.pacientes = pacientes;
    }

    public List<Familia> getClientes() {
        return clientes;
    }

    public void setClientes(List<Familia> clientes) {
        this.clientes = clientes;
    }
    
}
